#!/bin/bash

# Mencari waktu realtime
jam=$(date +"%H")

# Mengecek jika saat ini pukul 00.00 (dini hari)
if [[ "$jam" -eq 0 ]]; then
  # Set gambar yang didownload dengan jumlah = 1
	downloads=1
else
  # Set gambar yang didownload dengan jumlah sesuai jam yang real time yang sudah dideklarasi
	downloads="$jam"
fi

# Membuat direktori untuk hasil download file gambar
if [[ "$1" == "download" ]]; then

  # Deklarasi direktori baru
	direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
 	mkdir "$direktori"

  # Download gambar
	for ((i = 1;i <= downloads; i++)); do
		file="perjalanan_$i"
 		wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"  		
	done

elif [[ "$1" == "zip" ]]; then

# Mencari semua kumpulan direktori yang sudah ada
kumpdirektori=$(find . -type d -name "kumpulan_*")

# Membuat zip file dan dipindah ke direktori
zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"
zip -r "$zipfile" $kumpdirektori

else
# Menampilkan pesan dan keluar
echo "Usage: $0 [download|zip]"
exit 1
fi

# comment perintah konfigurasi pada crontab
# * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download
# 0 0 * * *  \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip
