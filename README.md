# Sisop-Praktikum-Modul-1-2023-FD dan MH-IT17

Nama kelompok : 
1. Salmaa Satifha Dewi Rifma Putri  (5027211011)
2. Yoga Hartono                     (5027211023)
3. Dzakirozaan Uzlahwasata          (5027211066)

# Soal 1
## Analisa Soal
• Menampilkan 5 Universitas dengan ranking tertinggi di Jepang
• Menampilkan Universitas di Jepang yang memiliki Faculty Student Score(fsr score) yang paling rendah
• Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
• Menampilkan Universitas paling keren di dunia dengan kata kunci keren

## Cara Pengerjaan Soal 1
• Menampilkan 5 Universitas dengan ranking tertinggi di Jepang
```sh
#!/bin/bash
grep "Japan" univ_rank.csv | sort -t , -k1 -g | head -5 | awk -F , '{print $1,$2}'
```
- gunakan `grep` untuk mencari kata "Japan" di dalam file `univ_rank.csv`.
- gunakan `sort -t , -k1 -g` untuk mensortir pada kolom 1, yaitu kolom rank keseluruhan dengan command `-k1`, untuk `-g` sendiri berfungsi untuk mengurutkan numerical yang bersifat nomor yang banyak seperti 600+
- gunakan `head -5` untuk print 5 baris pertama yang sudah di sort
- gunakan `awk -F , '{print $1,$2}'` untuk print hanya kolom 1 dan 2 saja

• Menampilkan Universitas di Jepang yang memiliki Faculty Student Score(fsr score) yang paling rendah
```sh
grep "Japan" univ_rank.csv | sort -t , -k9 -g | awk -F , '{print $1,$2,$9}' | head -1
```
- gunakan `-k9` untuk sort kolom 9, yaitu kolom yang berisi FSR Score
- gunakan `awk -F , '{print $1,$2,$9}'` untuk print kolom 1, 2, dan 9 saja
- gunakan `head -1` untuk print 1 baris pertama yang sudah di sort

• Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```sh
grep "Japan" univ_rank.csv | sort -t , -k20 -g | awk -F , '{print $1,$2,$20}' | head -10
```
- gunakan `-k20` untuk sort kolom 20, yaitu kolom yang berisi GER Rank
- gunakan `awk -F , '{print $1,$2,$20}'` untuk print kolom 1, 2, dan 9 saja
- gunakan `head -10` untuk print 10 baris pertama yang sudah di sort

• Menampilkan Universitas paling keren di dunia dengan kata kunci keren
```sh
grep -i "keren" univ_rank.csv | awk -F , '{print $1,$2}'
```
- gunakan `-i` untuk mencari kata "keren" pada file `univ_rank.csv`

## Source Code
```sh
echo "|5 Universitas dengan ranking tertinggi di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k1 -g | head -5 | awk -F , '{print $1,$2}'
echo
echo "|Universitas dengan FSR Score terendah di Jepang|\n"

grep "Japan" univ_rank.csv | sort -t , -k9 -g | awk -F , '{print $1,$2,$9}' | head -1
echo
echo "|10 Universitas di Jepang dengan GER Rank paling tinggi|\n"

grep "Japan" univ_rank.csv | sort -t , -k20 -g | awk -F , '{print $1,$2,$20}' | head -10
echo
echo "|Universitas paling keren di dunia|\n"

grep -i "keren" univ_rank.csv | awk -F , '{print $1,$2}'
```

## Test Output
![output-modul1](https://i.ibb.co/VqwNFcc/output-modul-1.png)

# Soal 2
## Analisis Soal
- Mendownload gambar tentang Indonesia sebanyak X kali dengan X sebagai jam sekarang dengan ketentuan : 
    - jika jam 0 mendownload 1 gambar saja
    - didownload 10 jam sekali
    - file bernama "perjalanan_nomor.file" (urutan file terdownload)
- File gambar dimasukkan dalam folder bernama "kumpulan_nomor.folder” (urutan folder terdownload)
- Mengubah format folder kumpulan menjadi zip dengan ketentuan :
    - terdownload ekstensi zip setiap 1 hari 
    - format nama zip “devil_nomor.zip” (urutan folder saat dibuat) 

## Cara Pengerjaan Soal 2
• Membuat folder soal2
```sh
mkdir soal2
```
• 2. Membuat dan membuka file script .sh dengan format nama kobeni_liburan.sh.
```sh
nano kobeni_liburan.sh
```
• Menentukan shell yang digunakan
```sh
#!/bin/bash
```
- baris pertama script ini menentukan shell yang akan digunakan untuk mengeksekusi script.

• Mencari waktu realtime
```sh
jam=$(date +"%H")
```
- baris ini menyimpan waktu real-time pada variabel jam yang dideklarasikan dengan format waktu `%H`(format 24 jam).

• Menentukan jumlah gambar yang didownload
```sh
# Mengecek jika saat ini pukul 00.00 (dini hari)
if [[ "$jam" -eq 0 ]]; then
  # Set gambar yang didownload dengan jumlah = 1
	downloads=1
else
  # Set gambar yang didownload dengan jumlah sesuai jam yang real time yang sudah dideklarasi
	downloads="$jam"
fi
```
- `if [[ "$jam" -eq 0 ]]; then`: ini adalah sebuah statement percabangan if. Jika jam sama dengan 0, maka akan dieksekusi perintah dalam blok if.
- `downloads=1`: jika saat ini pukul 00.00 (dini hari), downloads akan diset dengan angka 1.
- `else`: Jika jam tidak sama dengan 0, maka eksekusi akan dilanjutkan ke perintah yang ada dalam blok else.
- `downloads="$jam"`: di sini, downloads akan diset dengan angka yang sama dengan jam yang sedang berjalan.

• Membuat direktori untuk menyimpan hasil download an gambar
```sh
if [[ "$1" == "download" ]]; then

  # Deklarasi direktori baru
	direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
 	mkdir "$direktori"
```
• Mendownload gambar dari link
```sh
	for ((i = 1;i <= downloads; i++)); do
		file="perjalanan_$i"
 		wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"  		
	done
```
- `if [[ "$1" == "download" ]]; then`: ini adalah statement percabangan if lainnya. Ini memeriksa apakah argumen pertama yang diberikan saat menjalankan script adalah download.
- `direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"`: ini menyimpan string `"kumpulan_"` ditambah dengan nomor direktori baru di direktori. Nomor direktori baru dihitung dengan menjumlahkan 1 dengan jumlah direktori yang telah ada sebelumnya.
- `mkdir "$direktori"`: membuat direktori baru sesuai dengan nama yang didefinisikan dalam variabel `$direktori`.
- `for ((i = 1;i <= downloads; i++)); do`: perulangan for dimulai dengan menginisialisasi i dengan angka 1, dan dilakukan selama i kurang dari atau sama dengan jumlah downloads. i akan bertambah 1 setiap kali perulangan dilakukan.
- `file="perjalanan_$i"`: nama file yang disimpan dalam variabel file.
- `wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"`: ini mendownload gambar dari URL yang diberikan dengan menggunakan perintah wget. File disimpan dengan nama `perjalanan_$i` di direktori yang telah dibuat sebelumnya.

• Mengubah ekstensi folder yang sudah tersimpan menjadi format zip
```sh
elif [[ "$1" == "zip" ]]; then

# Mencari semua kumpulan direktori yang sudah ada
kumpdirektori=$(find . -type d -name "kumpulan_*")

# Membuat zip file dan dipindah ke direktori
zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"
zip -r "$zipfile" $kumpdirektori

else
# Menampilkan pesan dan keluar
echo "Usage: $0 [download|zip]"
exit 1
fi
```
- `elif [[ "$1" == "zip" ]]; then`: ini adalah statement percabangan elif. Jika argumen pertama yang diberikan saat menjalankan script adalah zip, maka perintah dalam blok elif akan dieksekusi.
- `kumpdirektori=$(find . -type d -name "kumpulan_*")`: mencari semua direktori yang telah dibuat sebelumnya dengan nama yang dimulai dengan "kumpulan_".
- `zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"`: nama file zip baru disimpan dalam variabel zipfile. Nama file tersebut diawali dengan "devil_", dan diikuti dengan nomor urut yang
berurutan dari 1,2,3 dan seterusnya.

• Membuat cron job
```sh
crontab -e
```
- membuat cron job dengan mengetik `crontab -e` pada terminal
- `crontab`: program untuk menjalankan tugas pada waktu tertentu secara otomatis
- `e`: opsi mengedit atau mengubah jadwal tugas yang sudah ada ataupun belum

• Me-set waktu yang digunakan untuk mendownload gambar dan mengubah menjadi format zip pada crontab
```sh
# comment perintah konfigurasi pada crontab
 * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download
 0 0 * * *  \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip
```
`- * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download`
Perintah ini akan dijalankan setiap 10 jam sekali, dimulai dari menit ke-0 setiap jamnya. Kemudian, `\\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download` merujuk pada file shell script kobeni_liburan.sh yang terletak pada direktori /home/alma/soal2 dan dengan argumen download.

`0 0 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip`
Perintah ini akan dijalankan setiap hari pukul 00:00. Kemudian, `\\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip` merujuk pada file shell script kobeni_liburan.sh yang terletak pada direktori /home/alma/soal2 dan dengan argumen zip.

Keduanya merujuk pada file shell script yang sama (`kobeni_liburan.sh`) yang akan dijalankan pada waktu yang sudah ditentukan untuk melakukan tugas-tugas tertentu, yaitu download gambar dan zip folder yang sudah dibuat oleh script.

## Source Code
```sh
#!/bin/bash

jam=$(date +"%H")

if [[ "$jam" -eq 0 ]]; then
	downloads=1
else
	downloads="$jam"
fi

if [[ "$1" == "download" ]]; then

	direktori="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
 	mkdir "$direktori"

	for ((i = 1;i <= downloads; i++)); do
		file="perjalanan_$i"
 		wget "https://id.wikipedia.org/wiki/Bali#/media/Berkas:TanahLot_2014.JPG" -O "$direktori/$file"  		
	done

elif [[ "$1" == "zip" ]]; then

kumpdirektori=$(find . -type d -name "kumpulan_*")

zipfile="devil_$(($(ls -d devil_* | wc -l) + 1)).zip"
zip -r "$zipfile" $kumpdirektori

else
echo "Usage: $0 [download|zip]"
exit 1
fi
```
```sh
 * */10 * * * \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh download
 0 0 * * *  \\wsl.localhost\Ubuntu\home\alma\soal2\kobeni_liburan.sh zip
```
## Test Ouput
- Ouput mendownload file gambar dan disimpan pada folder
![ouput21-modul1](https://i.ibb.co/yRnn72H/output-download.png)

- Output mengubah ekstensi folder menjadi zip
![output22-modul1](https://i.ibb.co/Cnh9PhT/output-zip.png)

## Kendala yang dialami 
1. Menemui kendala syntax error, kesalahan deklarasi variabel, dan kesalahan pemberian spasi pada code. Namun kendala sudah diatasi dengan melakukan perbaikan pada code. 
2. Menemui kendala hasil output “value too great for base (error token is "09")” ketika di bash, namun sudah diperbaiki dengan perbaikan code perintah pada crontab


# Soal 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh
- Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
    - Minimal 8 karakter
    - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    - Alphanumeric
    - Tidak boleh sama dengan username 
    - Tidak boleh menggunakan kata chicken atau ernie
- Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

# Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan







## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/praktikum-sisop-2023/sisop-praktikum-modul-1-2023-fd-dan-mh-it17.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/praktikum-sisop-2023/sisop-praktikum-modul-1-2023-fd-dan-mh-it17/-/settings/integrations)